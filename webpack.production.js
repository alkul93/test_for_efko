var webpack = require('webpack');

const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const aliases = require('./webpack.config.aliases.js');
var path = require('path');

module.exports = {
    entry: "./frontend/web/js/main.js",
    output: {
        path: path.resolve(__dirname, 'frontend/web/js/single/'),
        publicPath: '/frontend/web/js/single/',
        filename: 'bundle.js',
    },
    resolve: aliases,
    module: {
        loaders: [
            {
                test: /\.vue$/,
                loader: 'vue-loader',
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader'
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)(\?\S*)?$/,
                loader: 'file-loader'
            },
            {
                test: /\.(png|jpe?g|gif|svg)(\?\S*)?$/,
                loader: 'file-loader',
                query: {
                    name: '[name].[ext]?[hash]'
                }
            }
        ],
    },
    plugins: [
        new webpack.LoaderOptionsPlugin({
            minimize: true,
            debug: false
        }),
        new webpack.DefinePlugin({
            "process.env": {
                NODE_ENV: '"production"' // default value if not specified
            }
        }),
        new UglifyJSPlugin({
            uglifyOptions: {
                ie8: false,
                comments: false,
                beautify: false,
                mangle: {
                    screw_ie8: true,
                    keep_fnames: true
                },
                compress: {
                    screw_ie8: true,
                    ecma: 6
                },
                warnings: false
            },
            warningsFilter: (src) => true
        })
    ]
}
