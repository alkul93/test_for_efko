const aliases = require('./webpack.config.aliases.js');
var path = require('path');

module.exports = {
    entry: "./frontend/web/js/main.js",
    output: {
        path: path.resolve(__dirname, 'frontend/web/js/build/'),
        publicPath: '/frontend/web/js/build/',
        filename: 'bundle.js',
    },
    resolve: aliases,
    module: {
        loaders: [
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader'
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)(\?\S*)?$/,
                loader: 'file-loader'
            },
            {
                test: /\.(png|jpe?g|gif|svg)(\?\S*)?$/,
                loader: 'file-loader',
                query: {
                    name: '[name].[ext]?[hash]'
                }
            }
        ]
    },
}
