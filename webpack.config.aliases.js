var path = require('path');

module.exports = {
    alias: {
        /** Базовый путь */
        '@root': path.resolve(__dirname, "frontend/web/js"),

        /** Сокращенные пути к частоиспользуемым компонентам */
        '@common-components': path.resolve(__dirname, "frontend/web/js/vue/common-components"),
        '@mixins': path.resolve(__dirname, "frontend/web/js/vue/mixins"),
        '@classes': path.resolve(__dirname, "frontend/web/js/classes"),
        '@css': path.resolve(__dirname, "frontend/web/css"),
    },
    modules: [path.resolve(__dirname, "app"), "node_modules"]
}
