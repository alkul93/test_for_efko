<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'name' => $this->string()->comment('Имя пользователя'),
            'surname' => $this->string()->comment('Отчество'),
            'lastname' => $this->string()->comment('Фамилия'),
            'auth_key' => $this->string(32),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'role' => $this->integer()->comment('Роль пользователя'),
            'access_token' => $this->string()->comment('Токен доступа для пользователя'),

            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->insert('{{%user}}', [
            'username' => 'programmer1',
            'password_hash' => \Yii::$app->security->generatePasswordHash('programmer1'),
            'email' => 'programmer1@example.com',
            'lastname' => 'Иванов',
            'name' => 'Иван',
            'surname' => 'Иванович',
            'status' => \common\models\User::STATUS_ACTIVE,
            'role' => \common\models\User::ROLE_PROGRAMMER,
            'created_at' => (new \DateTime)->getTimestamp(),
            'updated_at' => (new \DateTime)->getTimestamp(),
        ]);

        $this->insert('{{%user}}', [
            'username' => 'programmer2',
            'password_hash' => \Yii::$app->security->generatePasswordHash('programmer2'),
            'email' => 'programmer2@example.com',
            'lastname' => 'Петров',
            'name' => 'Петр',
            'surname' => 'Петрович',
            'status' => \common\models\User::STATUS_ACTIVE,
            'role' => \common\models\User::ROLE_PROGRAMMER,
            'created_at' => (new \DateTime)->getTimestamp(),
            'updated_at' => (new \DateTime)->getTimestamp(),
        ]);

        $this->insert('{{%user}}', [
            'username' => 'leader',
            'password_hash' => \Yii::$app->security->generatePasswordHash('leader'),
            'email' => 'leader@example.com',
            'lastname' => 'Максимов',
            'name' => 'Максим',
            'surname' => 'Максимович',
            'status' => \common\models\User::STATUS_ACTIVE,
            'role' => \common\models\User::ROLE_LEADER,
            'created_at' => (new \DateTime)->getTimestamp(),
            'updated_at' => (new \DateTime)->getTimestamp(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}
