<?php

use yii\db\Migration;

/**
 * Handles the creation of table `vacation`.
 */
class m181128_110043_create_vacation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%vacation}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Идентификатор пользователя'),
            'start' => $this->integer()->comment('Дата начала отпуска'),
            'end' => $this->integer()->comment('Дата окончания отпуска'),
            'confirmed' => $this->boolean()->defaultValue(false)->comment('Отпуск подтвержден'),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%vacation}}');
    }
}
