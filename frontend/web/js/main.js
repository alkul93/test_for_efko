import Vue from 'vue/dist/vue.js'
import VueResource from 'vue-resource';
import VueRouter from 'vue-router';

import routes from './router/routes.js'
import User from './classes/User.js'

// ElementUI
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/ru-RU'
import 'element-ui/lib/theme-chalk/index.css'

import '@css/font-awesome-v4.7.min.css'
import '@css/site.css'

Vue.use(VueResource);
Vue.use(ElementUI, { locale });
Vue.use(VueRouter)

window.$ = window.jQuery = require('jquery')
window.moment = require('moment')
window.user = new User;

window.defaultUrl = "http://178.128.38.74:64321/"

/**
 * Добавляем дефолтный url перед каждым запросом.
 * @WARNING: ВАЖНО!!! Все урлы должны быть относительными: напр. api/v1/...
 */
Vue.url.options.root = window.defaultUrl


var router = new VueRouter({
    routes,
});

/**
 * Перед каждым запросом проверяем, если статус 401/403 редерект на страницу авторизации
 */
Vue.http.interceptors.push(function(request, next) {
    // continue to next interceptor
    next(function(response) {
        if(response.status == 401 || response.status == 403){
            router.push('/login')
        }
    });
 });

/**
 * Дополнительные заголовки перед каждым запросом добавляем токен
 */
Vue.http.interceptors.push(function(request) {
    let token = localStorage.getItem('token');

    request.headers.set('X-Api-Key', token)
    request.headers.set('content-type', 'application/json; charset=UTF-8')
});

/**
 * Перед каждым переходом на след. страницу проверяем не истекло ли время жизни
 * токена, и за одно получаем объект текущего пользователя.
 * Т.к. у нас пока еще не доступен vue-resource заюзаем обычный jq ajax
 */
router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        let token = localStorage.getItem('token');

        $.ajax({
            url: window.defaultUrl + 'api/auth/me',
            headers: {
                'X-Api-Key': token,
            },
            success: function(response){
                window.user.setUserParams(response.user);
                next();
            },
            error: function (response) {
                localStorage.removeItem('token');
                next({
                    path: '/login',
                    query: { redirect: to.fullPath }
                });
            }
        });
    } else {
        next();
    }
});

/* eslint-disable no-new */
var vm = new Vue({
    router
}).$mount('#app');
