var User = function () {

    return {
        id: '',
        full_name: '',
        role: '',
        is_auth: false,

        /**
         * Can a user perform an action
         *
         * @param string action
         * @return boolean
         */
        can: function (action) {
            /** To simplify */
            if (this.role == 'leader') {
                return true;
            }

            return false;
        },

        /**
         * Set user params
         *
         * @param array params
         * @return void
         */
        setUserParams(params) {
            this.id = params.id;
            this.full_name = params.fullname;
            this.role = params.role;
            this.is_auth = true;
        },

        /**
         * Deauthorize user
         *
         * @return void
         */
        deauthorize: function () {
            this.id = '';
            this.role = '';
            this.full_name = '';

            this.is_auth = false;
        },
    }
}

export default User;
