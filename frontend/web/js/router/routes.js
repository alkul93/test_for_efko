import NotFoundComponent from '@common-components/NotFoundComponent.vue';
import Header from '@common-components/Header.vue';
import StartPage from '@common-components/StartPage.vue';
import Login from '@common-components/Login.vue';
import VacationTable from '@root/vue/vacation/VacationTable.vue';
import VacationForm from '@root/vue/vacation/VacationForm.vue';

const routes = [
    {
        path: '*',
        components: {
            default: NotFoundComponent,
            header: Header,
        },
    },
    {
        path: '/',
        components: {
            default: StartPage,
            header: Header,
        },
    },
    {
        path: '/login',
        components: {
            default: Login,
            header: Header,
        },
    },
    {
        path: '/vacation',
        components: {
            default: VacationTable,
            header: Header,
        },
        meta: { requiresAuth: true }
    },
    {
        path: '/vacation/create',
        components: {
            default: VacationForm,
            header: Header,
        },
        meta: { requiresAuth: true }
    },
    {
        path: '/vacation/update/:id',
        components: {
            default: VacationForm,
            header: Header,
        },
        meta: { requiresAuth: true }
    },

]

export default routes;
