<?php
namespace api\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\User;
use yii\web\Response;


/**
 * Site controller
 */
class AuthController extends Controller
{
    /**
     * Авторизация через апи
     *
     * @return mixed
     */
    public function actionGetToken()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $request = \Yii::$app->request->post();
        $model = User::find()->where(['username' => $request['login'] ?? ''])->one();
        if(!$model) {
            Yii::$app->response->statusCode = 404;
            return ['message' => 'User not found'];
        }

        if ($model->validatePassword($request['password'])) {
            $model->access_token = Yii::$app->getSecurity()->generateRandomString();
            $model->save();

            return [
                'token' => $model->access_token,
                'user' => $model->getPublicUserData()
            ];
        }

        throw new \yii\web\ForbiddenHttpException();
    }

    /**
     * Получить текущего пользователя по токену
     *
     * @return array
     */
    public function actionMe()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $token = Yii::$app->request->headers['X-Api-Key'];
        $model = User::find()->where(['access_token' => $token])->one();
        if(!$model) {
            Yii::$app->response->statusCode = 401;
            return ['message' => 'Token not found or expired!'];
        }

        return ['user' => $model->getPublicUserData()];
    }

}
