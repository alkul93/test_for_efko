<?php
namespace api\tests;
use api\tests\ApiTester;

class GetTokenCest
{
    public function _before(ApiTester $I)
    {
    }

    // tests
    public function normalAuthTest(ApiTester $I)
    {
        $I->wantTo('Авторизация с нормальными данными пользователя');
        $I->sendPOST('/auth/get-token', ['login' => 'leader', 'password' => 'leader']);
        $I->seeResponseCodeIs(200);
    }

    public function emptyDataTest(ApiTester $I)
    {
        $I->wantTo('Авторизация с пустыми данными пользователя');
        $I->sendPOST('/auth/get-token', ['login' => '', 'password' => '']);
        $I->seeResponseCodeIs(404);
    }

    public function unknownUserAuthTest(ApiTester $I)
    {
        $I->wantTo('Авторизация с неизвестного системе пользователя');
        $I->sendPOST('/auth/get-token', ['login' => 'unknown', 'password' => 'aasd']);
        $I->seeResponseCodeIs(404);
    }

    public function passwordNoMatchTest(ApiTester $I)
    {
        $I->wantTo('Авторизация пользователя с неправильным паролем');
        $I->sendPOST('/auth/get-token', ['login' => 'leader', 'password' => 'qweqwe']);
        $I->seeResponseCodeIs(403);
    }
}
