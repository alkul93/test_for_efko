<?php namespace api\tests;
use api\tests\ApiTester;

class CreateVacationCest
{
    public function _before(ApiTester $I)
    {
    }

    // tests
    public function tryCreateVacationWithoutAuth(ApiTester $I)
    {
        $I->wantTo('Пробую добавить данные от неавторизованного пользователя');
        $I->sendPOST('/v1/vacation', [
            'start' => '1532972867',
            'end' => '1533972967',
        ]);

        $I->seeResponseCodeIs(401);
    }

    public function tryCreateVacationWithAuth(ApiTester $I)
    {
        $I->amAnProgrammer();

        $I->wantTo('Пробую добавить данные от авторизованного пользователя');
        $I->sendPOST('/v1/vacation', [
            'start' => '1532972867',
            'end' => '1533972967',
            'user_id' => $I->user->id,
        ]);

        $I->seeResponseCodeIs(201);
    }

    public function tryToAddInvalidData(ApiTester $I)
    {
        $I->amAnProgrammer();

        $I->wantTo('Пробую добавить данные без пользователя');
        $I->sendPOST('/v1/vacation', [
            'start' => '1532972867',
            'end' => '1533972967',
        ]);

        $I->seeResponseCodeIs(422);
        $I->seeResponseContainsJson(array('field' => 'user_id', 'message' => 'User ID cannot be blank.'));


        $I->wantTo('Пробую добавить данные от имени другого пользователя');
        $I->sendPOST('/v1/vacation', [
            'start' => '1532972867',
            'end' => '1533972967',
            'user_id' => $I->user->id + 10
        ]);

        $I->seeResponseCodeIs(422);
        $I->seeResponseContainsJson(array('field' => 'user_id', 'message' => 'User identity error!'));


        $I->wantTo('Пробую добавить незаполненые поля даты начала и даты окончания');
        $I->sendPOST('/v1/vacation', [
            'user_id' => $I->user->id,
        ]);

        $I->seeResponseCodeIs(422);
        $I->seeResponseContainsJson([
            ['field' => 'start', 'message' => 'Start date cannot be blank.'],
            ['field' => 'end', 'message' => 'End date cannot be blank.'],
        ]);


        $I->wantTo('Пробую добавить поля даты в неподходящем формате');
        $I->sendPOST('/v1/vacation', [
            'start' => '10.10.2018',
            'end' => '10.11.2018',
            'user_id' => $I->user->id,
        ]);

        $I->seeResponseCodeIs(422);
        $I->seeResponseContainsJson([
            ['field' => 'start', 'message' => 'Start date must be an unix timestamp.'],
            ['field' => 'end', 'message' => 'End date must be an unix timestamp.'],
        ]);


        $I->wantTo('Пробую добавить дату начала отпуска, большую чем дату окончания');
        $I->sendPOST('/v1/vacation', [
            'start' => '1532972868',
            'end' => '1532972867',
            'user_id' => $I->user->id,
        ]);

        $I->seeResponseCodeIs(422);
        $I->seeResponseContainsJson([
            ['field' => 'end', 'message' => "End date can't be greater than start date."],
        ]);

    }

}
