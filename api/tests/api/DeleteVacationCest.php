<?php namespace api\tests;
use api\tests\ApiTester;

class DeleteVacationCest
{
    public function _before(ApiTester $I)
    {
    }

    // tests
    public function tryToDeleteRecord(ApiTester $I)
    {
        $I->wantTo('Пробую удалить запись');
        $I->amAnProgrammer();
        $I->sendPOST('/v1/vacation', [
            'start' => '1532972867',
            'end' => '1533972967',
            'user_id' => $I->user->id,
        ]);

        $record = (object) $I->grabDataFromResponseByJsonPath('$.')[0];

        $I->sendDelete("/v1/vacation/{$record->id}", []);

        $I->seeResponseCodeIs(204);
        // TODO: вообще тут нужно было бы еще проверить что нам в ответ приходит

    }
}
