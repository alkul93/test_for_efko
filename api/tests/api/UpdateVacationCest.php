<?php namespace api\tests;
use api\tests\ApiTester;

class UpdateVacationCest
{
    public function _before(ApiTester $I)
    {
    }

    // tests
    public function tryToUpdateMyRecord(ApiTester $I)
    {
        $I->wantTo('Пробую обновить свою запись');
        $I->amAnProgrammer();
        $I->sendPOST('/v1/vacation', [
            'start' => '1532972867',
            'end' => '1533972967',
            'user_id' => $I->user->id,
        ]);

        $record = (object) $I->grabDataFromResponseByJsonPath('$.')[0];

        $I->sendPut("/v1/vacation/{$record->id}", [
            'start' => '1532972867',
            'end' => '1533972967',
        ]);

        $I->seeResponseCodeIs(200);
        // TODO: вообще тут нужно было бы еще проверить что нам в ответ приходит

    }

    public function tryToUpdateOtherRecord(ApiTester $I)
    {
        $I->wantTo('Пробую обновить чужую запись от имени программиста');
        $I->amAnProgrammer();

        $I->sendPOST('/v1/vacation', [
            'start' => '1532972867',
            'end' => '1533972967',
            'user_id' => $I->user->id,
        ]);

        $record = (object) $I->grabDataFromResponseByJsonPath('$.')[0];

        $I->amAnProgrammer2();

        $I->sendPut("/v1/vacation/{$record->id}", [
            'start' => '1532972867',
            'end' => '1533972967',
        ]);

        $I->seeResponseCodeIs(422);
    }

    public function tryToUpdateOtherRecordFromLeader(ApiTester $I)
    {
        $I->wantTo('Пробую обновить чужую запись от имени администратора');
        $I->amAnProgrammer();

        $I->sendPOST('/v1/vacation', [
            'start' => '1532972867',
            'end' => '1533972967',
            'user_id' => $I->user->id,
        ]);

        $record = (object) $I->grabDataFromResponseByJsonPath('$.')[0];

        $I->amAnLeader();

        $I->sendPut("/v1/vacation/{$record->id}", [
            'start' => '1532972867',
            'end' => '1533972967',
        ]);

        $I->seeResponseCodeIs(200);

    }

    public function tryToConfirmRecordFromLeader(ApiTester $I)
    {
        $I->wantTo('Пробую подтвердить отпуск от имени администратора');
        $I->amAnProgrammer();

        $I->sendPOST('/v1/vacation', [
            'start' => '1532972867',
            'end' => '1533972967',
            'user_id' => $I->user->id,
        ]);

        $record = (object) $I->grabDataFromResponseByJsonPath('$.')[0];

        $I->amAnLeader();

        $I->sendPut("/v1/vacation/{$record->id}", [
            'confirmed' => 1,
        ]);

        $I->seeResponseCodeIs(200);

    }

    public function tryToConfirmRecordFromProgrammer(ApiTester $I)
    {
        $I->wantTo('Пробую подтвердить отпуск от имени программиста');
        $I->amAnProgrammer();

        $I->sendPOST('/v1/vacation', [
            'start' => '1532972867',
            'end' => '1533972967',
            'user_id' => $I->user->id,
        ]);

        $record = (object) $I->grabDataFromResponseByJsonPath('$.')[0];

        $I->amAnProgrammer();

        $I->sendPut("/v1/vacation/{$record->id}", [
            'confirmed' => 1,
        ]);

        $I->seeResponseCodeIs(422);

    }


}
