<?php
namespace api\tests;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class ApiTester extends \Codeception\Actor
{
    use _generated\ApiTesterActions;

    /**
     * @var mixed Объект пользователя
     */
    public $user;


    /**
     * Define custom actions here
     */

    /**
     * Авторизованный пользователь
     *
     * @return void
     */
    public function amAnLeader()
    {
        $this->sendPOST('/auth/get-token', ['login' => 'leader', 'password' => 'leader']);
        $this->seeResponseCodeIs(200);
        $data = $this->grabDataFromResponseByJsonPath('$.token');
        $this->user = (object) $this->grabDataFromResponseByJsonPath('$.user')[0];
        $this->haveHttpHeader('X-Api-Key', $data[0]);
    }

    public function amAnProgrammer()
    {
        $this->sendPOST('/auth/get-token', ['login' => 'programmer1', 'password' => 'programmer1']);
        $this->seeResponseCodeIs(200);
        $data = $this->grabDataFromResponseByJsonPath('$.token');
        $this->user = (object) $this->grabDataFromResponseByJsonPath('$.user')[0];
        $this->haveHttpHeader('X-Api-Key', $data[0]);
    }

    public function amAnProgrammer2()
    {
        $this->sendPOST('/auth/get-token', ['login' => 'programmer2', 'password' => 'programmer2']);
        $this->seeResponseCodeIs(200);
        $data = $this->grabDataFromResponseByJsonPath('$.token');
        $this->user = (object) $this->grabDataFromResponseByJsonPath('$.user')[0];
        $this->haveHttpHeader('X-Api-Key', $data[0]);
    }

    /**
     * Логин пользователя
     *
     * @return string
     */
    public function myLogin()
    {
        return $this->login;
    }

    /**
     * Пароль пользователя
     *
     * @return string
     */
    public function myPassword()
    {
        return $this->password;
    }
}
