<?php

namespace api\modules\v1\controllers;


use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\filters\Cors;
use yii\rest\ActiveController;
use yii\web\Response;

class VacationController extends ActiveController
{
    public $modelClass = 'common\models\Vacation';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
        ];

        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];

        $behaviors['basicAuth'] = [
            'class' => \yii\filters\auth\HttpHeaderAuth::className(),
        ];

        return $behaviors;
    }

    /**
     * {@inheritdoc}
     */
    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
            'view' => ['GET', 'HEAD'],
            'create' => ['POST'],
            'update' => ['PUT', 'PATCH'],
            'delete' => ['DELETE'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        $actions = parent::actions();

        $actions['index'] = [
            'class' => 'api\actions\vacation\IndexAction'
        ];

        $actions['view'] = [
            'class' => 'api\actions\vacation\ViewAction'
        ];

        $actions['approve'] = [
            'class' => 'api\actions\vacation\ApproveAction'
        ];

        return $actions;
    }
}
