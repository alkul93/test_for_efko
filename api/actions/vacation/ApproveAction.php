<?php

namespace api\actions\vacation;

use Yii;
use yii\base\Action;
use yii\web\Response;

/**
 *
 */
class ApproveAction extends Action
{

    /**
     * {@inheritdoc}
     *
     * @param integer $id Идентификатор записи
     * @return array
     */
    public function run($id)
    {
        $model = $this->controller->modelClass::findOne($id);

        if (Yii::$app->user->identity->can('approve-vacation')) {
            $model->confirmed = true;
            $model->save();

            return ['message' => 'Success! Vacation approved!'];
        }

        Yii::$app->response->statusCode = 403;
        return ['message' => 'Forbidden'];

    }
}
