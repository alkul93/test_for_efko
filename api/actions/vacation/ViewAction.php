<?php

namespace api\actions\vacation;

use Yii;
use yii\base\Action;
use yii\web\Response;

/**
 *
 */
class ViewAction extends Action
{

    /**
     * {@inheritdoc}
     *
     * @param integer $id Идентификатор записи
     * @return array
     */
    public function run($id)
    {
        $model = $this->controller->modelClass::find()
            ->select(['id', 'start', 'end', 'user_id'])->where(['id' => $id])->one();

        // FIXME: So ugly
        $model->start = $model->start . '000';
        $model->end = $model->end . '000';

        return $model;
    }
}
