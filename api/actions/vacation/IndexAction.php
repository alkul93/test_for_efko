<?php

namespace api\actions\vacation;

use Yii;
use yii\base\Action;
use yii\web\Response;

/**
 *
 */
class IndexAction extends Action
{

    /**
     * {@inheritdoc}
     *
     * @param array $filters Массив фильтров
     * @return array
     */
    public function run(array $filters = [])
    {
        /**
         * FIXME: По хорошему тут, конечно, нужен компонент vacation,
         * Там должнен быть builder, который приведет к нужному формату (перед передачей
         * на фронт), модель поиска что бы учесть фильтры и т.д.
         */
        return $this->controller->modelClass::find()->with('user')->asArray()->all();
    }
}
