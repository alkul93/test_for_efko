<?php

namespace common\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "vacation".
 *
 * @property int $id
 * @property int $user_id Идентификатор пользователя
 * @property int $start Дата начала отпуска
 * @property int $end Дата окончания отпуска
 * @property int $confirmed Отпуск подтвержден
 * @property int $created_at
 * @property int $updated_at
 */
class Vacation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%vacation}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return \Yii::$app->id == 'app-console' ? [] :  [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => function ()
                {
                    return (new \DateTime)->getTimestamp();
                },
            ],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id',  'confirmed'], 'integer'],
            [['start', 'end', 'created_at', 'updated_at'], 'integer', 'message' => '{attribute} must be an unix timestamp.'],
            ['end', function ($attribute)
            {
                if ($this->$attribute <= $this->start) {
                    $this->addError($attribute, "End date can't be greater than start date.");
                }
            }],
            [['user_id', 'start', 'end'], 'required'],
            ['user_id', 'userValidate', 'when' => function ($model)
            {
                if (Yii::$app->id == 'app-console') {
                    return false;
                }

                return Yii::$app->user->identity->role == User::ROLE_PROGRAMMER;
            }],
            ['confirmed', function ($attribute)
            {
                if (Yii::$app->id == 'app-console') {
                    return true;
                }

                if ($this->$attribute && Yii::$app->user->identity->role == User::ROLE_PROGRAMMER) {
                    $this->addError("Only administrators can confirm the vacation!");
                }
            }],
            [['start', 'end'], 'filter', 'filter' => function ($attribute)
            {
                if (mb_strlen($attribute) > 10) {
                    return mb_substr($attribute, 0, -3);
                }

                return $attribute;
            }]
        ];
    }

    /**
     * Валидация, что бы пользователь не мог добавить отпуск от другого отпуска
     *
     * @param string $attribute
     * @return boolean|void
     */
    public function userValidate($attribute)
    {
        if (Yii::$app->id == 'app-console' || Yii::$app->user->id == $this->$attribute) {
            return true;
        }

        $this->addError($attribute, "User identity error!");
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'start' => 'Start date',
            'end' => 'End date',
            'confirmed' => 'Confirmed',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }


    /**
     * Relation to user table
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'user_id']);
    }
}
